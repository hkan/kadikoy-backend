import template from './template.html'

export default Vue.extend({
    template,

    data: function () {
        return {
            posts: []
        }
    },

    ready: function () {
        var $vm = this

        setTimeout(function () {
            $vm.posts.push({
                id: 1,
                body: 'asdsasd',
                upvotes: 0,
                downvotes: 0
            })
        }, 3000)
    }
})
