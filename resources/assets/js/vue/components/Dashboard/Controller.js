import template from './template.html'
import PostListController from '../PostList/Controller.js'

export default Vue.extend({
    template,

    components: {
        'posts': PostListController
    }
})
