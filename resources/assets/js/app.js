(function () {
    window.App = {};

    Vue.config.debug = true;
    Vue.config.delimiters = ['${', '}'];
    Vue.config.unsafeDelimiters = ['$!{', '}'];

    Vue.use(VueRouter);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})();