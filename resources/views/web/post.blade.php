@extends('web.master')

@section('body')

    <div class="container">
        <div class="owner">
            <a href="{{ route('user', $post->op->id) }}">
                {{ $post->op->name }}
            </a>
        </div>

        <div class="category">
            <a href="{{ route('category', $post->category->id) }}">
                {{ $post->category->name }}
            </a>
        </div>

        <p>{{ $post->body }}</p>
    </div>

@endsection