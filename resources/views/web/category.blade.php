@extends('web.master')

@section('body')

    <div class="container">
        <div class="page-header">
            <h1>{{ $category->name }}</h1>
        </div>

        @foreach ($category->posts as $post)
            <div class="post">
                <div class="owner">
                    <a href="{{ route('user', $post->op->id) }}">
                        {{ $post->op->name }}
                    </a>
                </div>

                <div>
                    <a href="{{ route('category', $post->category->id) }}">{{ $post->category->name }}</a>
                </div>

                <div>
                    <a href="{{ route('post', $post->id) }}">
                        {{ $post->body }}
                    </a>
                </div>
            </div>
        @endforeach
    </div>

@endsection