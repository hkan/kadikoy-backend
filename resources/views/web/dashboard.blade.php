@extends('web.master')

@section('body')

    <div class="container">
        <div class="page-header">
            <h1>Postlarınız</h1>
        </div>

        @foreach ($posts as $post)

            <div class="post">
                <div class="owner">
                    <div>
                        <a href="{{ route('post', $post->id) }}">
                            {{ $post->body }}
                        </a>
                    </div>

                    <div>
                        <a href="{{ route('category', $post->category->id) }}">
                            {{ $post->category->name }}
                        </a>
                        kategorisinde
                    </div>
                </div>
            </div>

        @endforeach
    </div>

@endsection