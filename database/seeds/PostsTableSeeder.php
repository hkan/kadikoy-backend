<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Post::create([
            'body' => 'First posts content.',
            'category_id' => 1,
            'sent_by' => 1
        ]);
    }
}
