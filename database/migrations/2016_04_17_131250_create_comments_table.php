<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');

            // Post
            $table->integer('post_id')->unsigned()->nullable();
            $table->foreign('post_id')->references('id')->on('posts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            // Owner
            $table->integer('sent_by')->unsigned()->nullable();
            $table->foreign('sent_by')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            // Body
            $table->text('body');

            // Votes
            $table->mediumInteger('upvotes')->default(0);
            $table->mediumInteger('downvotes')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
