<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table)
        {
            $table->increments('id');

            // Category
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')
                ->onDelete('set null');

            // OP
            $table->integer('sent_by')->unsigned()->nullable();
            $table->foreign('sent_by')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            // Content
            $table->longText('body');

            // Image
            $table->string('image')->nullable();

            // Votes
            $table->mediumInteger('upvotes')->default(0);
            $table->mediumInteger('downvotes')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
