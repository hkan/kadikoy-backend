var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass([
        'app.scss'
    ], 'public/assets/css');

    mix.scripts([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        'node_modules/underscore/underscore.js',
        'node_modules/vue-router/dist/vue-router.js',
        'node_modules/vue/dist/vue.js',
        'node_modules/moment/min/moment.min.js',
        'node_modules/moment/locale/tr.js',
        'resources/assets/js/*.js',
        '!resources/assets/js/_*.js'
    ], 'public/assets/js', './');

    mix.copy('node_modules/font-awesome/fonts', 'public/assets/fonts');
    mix.copy('node_modules/socket.io-client/socket.io.js', 'public/assets/js');

    mix.browserify('vue/app.js', elixir.config.publicPath + '/assets/js/vue/app.js');
});
