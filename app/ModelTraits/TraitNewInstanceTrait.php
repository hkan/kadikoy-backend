<?php

namespace App\ModelTraits;

trait TraitNewInstanceTrait
{
    /**
     * @param array $attributes
     * @param bool|false $exists
     * @return void|static
     */
    public function newInstance($attributes = [], $exists = false)
    {
        $model = parent::newInstance($attributes, $exists);

        foreach (class_uses_recursive(get_called_class()) as $trait) {
            if (method_exists(get_called_class(), $method = 'newInstance'.class_basename($trait))) {
                call_user_func([get_called_class(), $method], $model);
            }
        }

        return $model;
    }
}