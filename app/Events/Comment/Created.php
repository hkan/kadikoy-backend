<?php

namespace App\Events\Comment;

use App\Comment;
use App\Events\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;

class Created extends Event implements ShouldBroadcastNow
{
    use SerializesModels;

    /**
     * @var Comment
     */
    public $comment;

    /**
     * Create a new event instance.
     *
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the name the event should be broadcast as.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'comments.created';
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['default'];
    }
}
