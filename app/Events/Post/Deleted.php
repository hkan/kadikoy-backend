<?php

namespace App\Events\Post;

use App\Events\Event;
use App\Post;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;

class Deleted extends Event implements ShouldBroadcastNow
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * Create a new event instance.
     *
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Broadcast data.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'post' => [
                'id' => $this->post->id
            ]
        ];
    }

    /**
     * Get the name the event should be broadcast as.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'posts.deleted';
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['default'];
    }
}
