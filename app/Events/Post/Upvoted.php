<?php

namespace App\Events\Post;

use App\Events\Event;
use App\Post;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;

class Upvoted extends Event implements ShouldBroadcastNow
{
    use SerializesModels;

    /**
     * @var Post
     */
    public $post;

    /**
     * Create a new event instance.
     *
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Get the name the event should be broadcast as.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'posts.upvoted';
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['default'];
    }
}
