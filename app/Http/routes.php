<?php

Route::pattern('id', '\d+');
Route::pattern('posts', '\d+');
Route::pattern('categories', '\d+');
Route::pattern('comments', '\d+');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web'], 'namespace' => 'Web'], function ()
{
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@home']);

    Route::get('{any}', 'HomeController@app')->where('any', '.+');
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@dashboard']);
    Route::get('post/{id}', ['as' => 'post', 'uses' => 'PostController@show']);
    Route::get('category/{id}', ['as' => 'category', 'uses' => 'CategoryController@show']);
    Route::get('user/{id}', ['as' => 'user', 'uses' => 'UserController@show']);
});

Route::group(['middleware' => ['api'], 'namespace' => 'Api', 'prefix' => 'api'], function ()
{
    Route::get('categories', 'CategoryController@index');

    Route::get('categories/{categories}', 'CategoryController@show');

    Route::resource('posts', 'PostController', [
        'only' => ['show', 'store', 'destroy']
    ]);

    Route::post('posts/{posts}/upvote', 'PostController@upvote');
    Route::post('posts/{posts}/downvote', 'PostController@downvote');

    Route::resource('posts.comments', 'CommentController', [
        'only' => ['index', 'store', 'destroy']
    ]);

    Route::post('posts/{posts}/comments/{comments}/upvote', 'CommentController@upvote');
    Route::post('posts/{posts}/comments/{comments}/downvote', 'CommentController@downvote');
});
