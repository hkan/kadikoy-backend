<?php

namespace App\Http\Controllers\Web;

use App\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function show($id)
    {
        /** @var Category $category */
        $category = Category::find($id);

        if (is_null($category))
        {
            throw new ModelNotFoundException('Could not find category.');
        }

        $category->load('posts', 'posts.op');

        return view('web.category', compact('category'));
    }
}
