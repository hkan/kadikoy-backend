<?php

namespace App\Http\Controllers\Web;

use App\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function show($id)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not find post.');
        }

        $post->load('op', 'category', 'comments', 'comments.owner');

        return view('web.post', compact('post'));
    }
}
