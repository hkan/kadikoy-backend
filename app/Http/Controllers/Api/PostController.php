<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Comment;
use App\Exceptions\PostAlreadyVotedException;
use App\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Show post and its comments.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not found post.');
        }

        $post->load('category', 'comments', 'comments.owner');

        return response()->json([
            'post' => $this->transformPost($post),
            'comments' => $post->comments->map([$this, 'transformComment'])
        ]);
    }

    /**
     * Create new post.
     *
     * @param  Requests\Api\Post\CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Requests\Api\Post\CreateRequest $request)
    {
        /** @var \App\Post $post */
        $post = $this->dispatch(
            new \App\Jobs\Post\Create(
                ['sent_by' => 1] + $request->only('category_id', 'body')
            )
        );

        if (is_null($post))
        {
            throw new \Exception('Could not create post.');
        }

        if ($request->hasFile('image'))
        {
            $this->dispatch(
                new \App\Jobs\Post\ProcessImage(
                    $request->file('image'),
                    $post
                )
            );
        }

        return response()->json($post);
    }

    /**
     * Give upvote to a post.
     *
     * @param Request $request
     * @param $postID
     * @return \Illuminate\Http\JsonResponse
     */
    public function upvote(Request $request, $postID)
    {
        /** @var Post $post */
        $post = Post::find($postID);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not find post.');
        }

        try
        {
            $upvoted = $this->dispatch(
                new \App\Jobs\Post\Upvote(
                    $post,
                    $request->user()
                )
            );
        }
        catch (PostAlreadyVotedException $e)
        {
            $upvoted = true;
        }

        return response()->json([
            'result' => $upvoted,
            'post' => $post
        ]);
    }

    /**
     * Give downvote to a post.
     *
     * @param Request $request
     * @param $postID
     * @return \Illuminate\Http\JsonResponse
     */
    public function downvote(Request $request, $postID)
    {
        $post = Post::find($postID);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not find post.');
        }

        try
        {
            $downvoted = $this->dispatch(
                new \App\Jobs\Post\Downvote(
                    $post,
                    $request->user()
                )
            );
        }
        catch (PostAlreadyVotedException $e)
        {
            $downvoted = true;
        }

        return response()->json([
            'result' => $downvoted,
            'post' => $post
        ]);
    }

    /**
     * @param  Requests\Api\Post\DeleteRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Requests\Api\Post\DeleteRequest $request, $id)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not find post.');
        }

        $deleted = $this->dispatch(
            new \App\Jobs\Post\Delete($post)
        );

        return response()->json([
            'result' => $deleted
        ]);
    }

    /**
     * @param  Post  $post
     * @return Post
     */
    protected function transformPost(Post $post)
    {
        if ($post->relationLoaded('category'))
        {
            $post->category->setVisible([
                'id',
                'name'
            ]);
        }

        return $post->setVisible([
            'id',
            'op',
            'body',
            'image',
            'upvotes',
            'downvotes',
            'category',
            'created_at'
        ]);
    }

    /**
     * @param  Comment  $comment
     * @return Comment
     */
    public function transformComment(Comment $comment)
    {
        if ($comment->relationLoaded('owner'))
        {
            $comment->owner->setVisible([
                'id',
                'username',
                'avatar'
            ]);
        }

        return $comment->setVisible([
            'id',
            'owner',
            'body',
            'upvotes',
            'downvotes',
            'created_at'
        ]);
    }
}
