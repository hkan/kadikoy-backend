<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * @param $postID
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($postID)
    {
        /** @var Post $post */
        $post = Post::find($postID);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not find post.');
        }

        $post->load('comments', 'comments.owner');

        return response()->json([
            'comments' => $post->comments->map([$this, 'transformComment'])
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\Api\Comment\CreateRequest $request
     * @param  int $postID
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Api\Comment\CreateRequest $request, $postID)
    {
        /** @var Post $post */
        $post = Post::find($postID);

        if (is_null($post))
        {
            throw new ModelNotFoundException('Could not find post.');
        }

        $comment = $this->dispatch(
            new \App\Jobs\Comment\Create(
                $request->only('body'),
                $request->user(),
                $post
            )
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Requests\Api\Comment\DeleteRequest $request
     * @param $postID
     * @param $commentID
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Requests\Api\Comment\DeleteRequest $request, $postID, $commentID)
    {
        //
    }

    /**
     * @param  Comment  $comment
     * @return Comment
     */
    public function transformComment(Comment $comment)
    {
        if ($comment->relationLoaded('owner'))
        {
            $comment->owner->setVisible([
                'id',
                'username',
                'avatar'
            ]);
        }

        return $comment->setVisible([
            'id',
            'owner',
            'body',
            'upvotes',
            'downvotes',
            'created_at'
        ]);
    }
}
