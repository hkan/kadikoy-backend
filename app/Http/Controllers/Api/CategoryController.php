<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        return response()->json([
            'data' => Category::all()->map([$this, 'transformCategory'])
        ], 200);
    }

    /**
     * @param  Request $request
     * @param  int $categoryID
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $categoryID)
    {
        $category = Category::find($categoryID);

        $posts = $category->posts()
            ->with('op')
            ->take(2)
            ->get();

        return response()->json([
            'category' => $this->transformCategory($category),
            'posts' => $posts->map([$this, 'transformPost'])
        ], 200);
    }

    /**
     * @param  Post  $post
     * @return  Post
     */
    public function transformPost(Post $post)
    {
        if ($post->relationLoaded('op'))
        {
            $post
                ->op
                ->setVisible([
                    'id',
                    'name',
                    'avatar'
                ]);
        }

        return $post
            ->setVisible([
                'id',
                'op',
                'body',
                'image',
                'upvotes',
                'downvotes',
                'created_at'
            ]);
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function transformCategory(Category $category)
    {
        return $category
            ->setVisible([
                'id',
                'name'
            ]);
    }
}
