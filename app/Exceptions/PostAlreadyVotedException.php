<?php

namespace App\Exceptions;

class PostAlreadyVotedException extends \LogicException
{
}