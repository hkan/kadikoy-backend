<?php

namespace App\Jobs\Post;

use App\Exceptions\PostAlreadyVotedException;
use App\Jobs\Job;
use App\Post;
use App\User;
use App\Vote;

class Upvote extends Job
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * @var User
     */
    protected $user;

    /**
     * Create a new job instance.
     *
     * @param Post $post
     * @param User $user
     */
    public function __construct(Post $post, User $user)
    {
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        $vote = $this->previousVote();

        $this->checkIfAlreadyVoted($vote);

        \DB::beginTransaction();

        if ($this->isCounterVoted($vote))
        {
            if (!$this->flipVote($vote))
            {
                \DB::rollBack();

                return false;
            }
        }
        else
        {
            $vote = $this->createNewVote();

            if (is_null($vote))
            {
                \DB::rollBack();

                return false;
            }
        }

        if (!$this->savePost())
        {
            \DB::rollBack();

            return false;
        }

        \DB::commit();

        event(new \App\Events\Post\Upvoted($this->post));

        return true;
    }

    /**
     * @return null|Vote
     */
    protected function previousVote()
    {
        return $this->post
            ->votes()
            ->where('voted_by', $this->user->id)
            ->first();
    }

    /**
     * @param $vote
     */
    protected function checkIfAlreadyVoted($vote)
    {
        if (!is_null($vote) && $vote->is_positive)
        {
            throw new PostAlreadyVotedException();
        }
    }

    /**
     * @param $vote
     * @return bool
     */
    protected function isCounterVoted($vote)
    {
        return !is_null($vote) && !$vote->is_positive;
    }

    /**
     * @param $vote
     */
    protected function flipVote($vote)
    {
        $this->post->downvotes -= 1;

        $vote->is_positive = true;

        return $vote->save();
    }

    /**
     * @return null|Vote
     */
    protected function createNewVote()
    {
        return $this->post->votes()->create([
            'voted_by' => $this->user->id,
            'is_positive' => true
        ]);
    }

    /**
     * @return bool
     */
    protected function savePost()
    {
        $this->post->upvotes += 1;

        return $this->post->save();
    }
}
