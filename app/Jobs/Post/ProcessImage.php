<?php

namespace App\Jobs\Post;

use App\Jobs\Job;
use App\Post;
use Illuminate\Http\UploadedFile;

class ProcessImage extends Job
{
    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @var Post
     */
    protected $post;

    /**
     * Create a new job instance.
     *
     * @param  UploadedFile  $file
     * @param  Post  $post
     */
    public function __construct(UploadedFile $file, Post $post)
    {
        $this->file = $file;
        $this->post = $post;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $dir = public_path('uploads/posts/' . $this->post->id);

        if (!is_dir($dir))
        {
            if (!mkdir($dir, 0755, true))
            {
                return false;
            }
        }

        $name = 'image.jpg';
        $path = rtrim($dir, '/') . '/' . $name;

        app('image')
            ->make($this->file)
            ->resize(960, null, function ($constraint)
            {
                $constraint->aspectRatio();
            })
            ->save($path);

        $this->post->image = str_replace(public_path() . '/', '', $path);

        $this->post->save();

        return true;
    }
}
