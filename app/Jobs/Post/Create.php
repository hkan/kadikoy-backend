<?php

namespace App\Jobs\Post;

use App\Jobs\Job;
use App\Post;

class Create extends Job
{
    /**
     * @var array
     */
    protected $data;

    /**
     * Whether the event should be fired.
     *
     * @var bool
     */
    protected $fireEvent = true;

    /**
     * Create a new job instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return null|\App\Post
     */
    public function handle()
    {
        $post = Post::create($this->data);

        if (is_null($post))
        {
            return null;
        }

        if ($this->fireEvent)
        {
            event(new \App\Events\Post\Created($post));
        }

        return $post;
    }

    /**
     * @param  bool  $event
     * @return $this
     */
    public function withEvent($event = true)
    {
        $this->fireEvent = (bool) $event;

        return $this;
    }
}
