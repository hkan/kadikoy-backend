<?php

namespace App\Jobs\Post;

use App\Jobs\Job;
use App\Post;

class Delete extends Job
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * Create a new job instance.
     *
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        \DB::beginTransaction();

        $deleted = $this->post->delete();

        if (!$deleted)
        {
            \DB::rollBack();

            return false;
        }

        event(new \App\Events\Post\Deleted($this->post));

        \DB::commit();

        return true;
    }
}
