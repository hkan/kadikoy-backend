<?php

namespace App\Jobs\Comment;

use App\Comment;
use App\Jobs\Job;
use App\Post;
use App\User;

class Create extends Job
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var Post
     */
    protected $post;

    /**
     * @var User
     */
    protected $owner;

    /**
     * Create a new job instance.
     * @param array $data
     * @param User $owner
     * @param Post $post
     */
    public function __construct(array $data, User $owner, Post $post)
    {
        $this->data = $data;
        $this->post = $post;
        $this->owner = $owner;
    }

    /**
     * Execute the job.
     *
     * @return null|Comment
     */
    public function handle()
    {
        /** @var Comment $comment */
        $comment = $this->post->comments()->create($this->data());

        if (!is_null($comment))
        {
            event(new \App\Events\Comment\Created($comment));
        }

        return $comment;
    }

    /**
     * @return array
     */
    protected function data()
    {
        return ['sent_by' => $this->owner->id] + $this->data;
    }
}
